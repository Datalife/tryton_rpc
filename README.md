Tryton RPC
==========

Tools for connect to Tryton server via RPC.


Installation
============

To install tryton_rpc, clone the project and run the setup.py script. The following line works on Linux, other OS not tested:

    sudo ./setup.py install


License
=======

See LICENSE file
