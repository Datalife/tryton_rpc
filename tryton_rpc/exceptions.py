# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from xmlrpc.client import Fault as RPCFault
__all__ = ['TrytonRPCError', 'Fault', 'ServerUnavailable']


class TrytonRPCError(Exception):
    pass


class ServerUnavailable(TrytonRPCError):
    pass


class Fault(TrytonRPCError, RPCFault):

    def __init__(self, faultCode, faultString='', **extra):
        super(Fault, self).__init__(faultCode, faultString, **extra)
        self.args = faultString

    def __repr__(self):
        return ("<Fault %s: %s>" %
            (repr(self.faultCode), repr(self.faultString)))
